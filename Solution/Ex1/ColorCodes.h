#pragma once

#define BLACK_CC "\033[30m"
#define RED_CC "\033[31m"
#define GREEN_CC "\033[32m"
#define YELLOW_CC "\033[33m"
#define BLUE_CC "\033[34m"
#define MAGENTA_CC "\033[35m"
#define CYAN_CC "\033[36m"
#define WHITE_CC "\033[37m"

#define RESET_CC "\033[0m"
#define BOLD_CC "\033[1m"
#define UNDERLINE_CC "\033[4m"
#define INVERSE_CC "\033[7m"
#define DARK_CC "\033[21m"
#define NO_LINE_CC "\033[24m"
#define NO_INVERSE_CC "\033[27m"