#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include "ColorCodes.h"
#include <iostream>
#include <exception>

using namespace std;

int main()
{
	system("cls");

	try
	{
		WSAInitializer wsaInit;
		Server server;
		server.serve(8826);
	} 
	catch(exception e)
	{
		cout << RESET_CC << RED_CC << "ERROR! Info: " << BOLD_CC << e.what() << RESET_CC << endl;
	}
	 
	system("pause");

	return 0;
}