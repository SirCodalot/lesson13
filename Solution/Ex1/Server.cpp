#include "Server.h"

#include <mutex>
#include <fstream>
#include <iostream>
#include <exception>
#include <string>
#include <thread>

#include "Client.h"
#include "ColorCodes.h"

Server::Server()
{
	_clients = *new deque<string>();
	_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_socket == INVALID_SOCKET)
	{
		throw exception(__FUNCTION__ " - socket");
	}

	ifstream read;
	read.open("shared_doc.txt");
	_document = string((std::istreambuf_iterator<char>(read)), std::istreambuf_iterator<char>());
	read.close();
}

Server::~Server()
{
	ofstream write;
	write.open("shared_doc.txt");
	write << _document;
	write.close();

	try
	{
		closesocket(_socket);
	}
	catch (...) {}
}

void Server::serve(int port) const
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port);
	sa.sin_family = AF_INET;
	sa.sin_addr.S_un.S_addr = INADDR_ANY;

	if (::bind(_socket, (sockaddr*) &sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw exception(__FUNCTION__ " - bind");
	}
	
	if (listen(_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw exception(__FUNCTION__ " - listen");
	}

	cout << RESET_CC << BLACK_CC << BOLD_CC << "Listening on port " << WHITE_CC << port << RESET_CC << endl;
	
	accept();
}

void Server::accept() const
{
	while (true)
	{
		std::cout << endl << RESET_CC << BLACK_CC << BOLD_CC << "Waiting for a Client..." << RESET_CC << std::endl;
		
		SOCKET client = ::accept(_socket, 0, 0);

		if (client == INVALID_SOCKET)
		{
			cout << RESET_CC << RED_CC << "Invalid Socket" << endl;
			continue;
		}

		SocketInfo info = { (Server*)this, client };
		thread th(newClient, info);
		th.detach();
	}
}

int Server::locateClient(string client) const
{
	for (int i = 0; i < client.size(); i++)
	{
		if (client == _clients[i])
		{
			return i;
		}
	}
	return -1;
}

bool Server::isLoggedIn(string client) const
{
	for (string c : _clients)
	{
		if (c == client)
		{
			return true;
		}
	}
	return false;
}

void Server::addToQueue(string client)
{
	_clients.push_back(client);
}

void Server::removeFromQueue(string client)
{
	for (int i = 0; i < _clients.size(); i++)
	{
		if (_clients[i] == client)
		{
			_clients.erase(_clients.begin() + i);
			break;
		}
	}
}

string Server::progressQueue()
{
	string client = _clients.front();
	_clients.pop_front();
	_clients.push_back(client);
	return client;
}


SOCKET Server::getSocket() const
{
	return _socket;
}

deque<string> Server::getClients() const
{
	return _clients;
}

string Server::getDocument() const
{
	return _document;
}

void Server::setDocument(string document)
{
	_document = document;

	ofstream write;
	write.open("shared_doc.txt");
	write << _document;
	write.close();
}
