#include "Client.h"
#include "ColorCodes.h"
#include "Helper.h"

#include <chrono>
#include <thread>
#include <iostream>
#include <fstream>

Client* newClient(SocketInfo info)
{
	cout << RESET_CC << BLACK_CC << BOLD_CC << "Successfully found a client." << RESET_CC << endl;

	return new Client(info);
}

void updateAll(Server* server, string ignore)
{
	string document = server->getDocument();
	string first = !server->getClients().empty() ? server->getClients()[0] : "";
	string second = server->getClients().size() >= 2 ? server->getClients()[1] : "";

	for (Client* client : clients)
	{
		if (client->getName() == ignore)
		{

			continue;
		}

		cout << RESET_CC << GREEN_CC << DARK_CC;
		Helper::sendUpdateMessageToClient(client->getSocket(), document, first, second, server->locateClient(client->getName()) + 1);
		cout << RESET_CC;
	}
}

Client::Client(SocketInfo info)
{
	_name = "";
	_server = info._server;
	_socket = info._socket;
	
	try
	{
		communicate();
	} 
	catch (...)
	{
		closesocket(_socket);
	}
}

Client::~Client()
{
	try 
	{
		closesocket(_socket);
	} 
	catch (...) {}
}

string Client::getName() const
{
	return _name;
}

SOCKET Client::getSocket() const
{
	return _socket;
}

void Client::update()
{
	string document = _server->getDocument();
	string first = !_server->getClients().empty() ? _server->getClients()[0] : "";
	string second = _server->getClients().size() >= 2 ? _server->getClients()[1] : "";

	cout << RESET_CC << GREEN_CC << DARK_CC;
	Helper::sendUpdateMessageToClient(_socket, document, first, second, _server->locateClient(_name) + 1);
	cout << RESET_CC;
}

void Client::communicate()
{
	while (true)
	{
		try
		{
			int type = Helper::getMessageTypeCode(_socket);

			switch (type)
			{
			case MT_CLIENT_LOG_IN:
				loginRequest();
				update();
				break;
			case MT_CLIENT_UPDATE:
				updateDocument();
				updateAll(_server, "");
				break;
			case MT_CLIENT_FINISH:
				_server->progressQueue();
				updateDocument();
				updateAll(_server, "");
				break;
			case MT_CLIENT_EXIT:
				_server->removeFromQueue(_name);

				for (int i = 0; i < clients.size(); i++)
				{
					if (clients[i]->getName() == _name)
					{
						clients.erase(clients.begin() + i);
						break;
					}
				}

				closesocket(_socket);
				updateAll(_server, _name);
				break;
			}

		} catch (...) {}
	}
}

void Client::loginRequest()
{
	int size = Helper::getIntPartFromSocket(_socket, 2);
	_name = Helper::getStringPartFromSocket(_socket, size);
	_server->addToQueue(_name);
	clients.push_back(this);
}

void Client::updateDocument()
{
	int size = Helper::getIntPartFromSocket(_socket, 5);
	string content = Helper::getStringPartFromSocket(_socket, size);

	_server->setDocument(content);
}
